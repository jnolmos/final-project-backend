import { Repository } from 'typeorm';

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CreateEspecieDto } from './dto/create-especie.dto';
import { UpdateEspecieDto } from './dto/update-especie.dto';
import { Especie } from './entities/especie.entity';

@Injectable()
export class EspeciesService {
  constructor(
    @InjectRepository(Especie)
    private especiesRepository: Repository<Especie>,
  ) {}

  async create(createEspecieDto: CreateEspecieDto) {
    try {
      const especieNombre = await this.especiesRepository.findOneBy({
        nombre: createEspecieDto.nombre,
      });

      if (especieNombre) {
        throw new Error(`${createEspecieDto.nombre} ya existe.`);
      }

      return this.especiesRepository.save(createEspecieDto);
    } catch (error) {
      return { code: 0, message: error.message };
    }
  }

  findAll() {
    return this.especiesRepository.find();
  }

  findOne(id: string) {
    return this.especiesRepository.findOneBy({ id });
  }

  update(id: number, updateEspecieDto: UpdateEspecieDto) {
    return `This action updates a #${id} especie`;
  }

  remove(id: string) {
    return this.especiesRepository.delete(id);
  }
}
