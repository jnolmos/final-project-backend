import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';

import { CreateEspecieDto } from './dto/create-especie.dto';
import { UpdateEspecieDto } from './dto/update-especie.dto';
import { EspeciesService } from './especies.service';

@Controller('especies')
export class EspeciesController {
  constructor(private readonly especiesService: EspeciesService) {}

  @Post()
  create(@Body() createEspecieDto: CreateEspecieDto) {
    return this.especiesService.create(createEspecieDto);
  }

  @Get()
  findAll() {
    return this.especiesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.especiesService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateEspecieDto: UpdateEspecieDto) {
    return this.especiesService.update(+id, updateEspecieDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.especiesService.remove(id);
  }
}
