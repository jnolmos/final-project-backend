import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Especie } from './entities/especie.entity';
import { EspeciesController } from './especies.controller';
import { EspeciesService } from './especies.service';

@Module({
  imports: [TypeOrmModule.forFeature([Especie])],
  controllers: [EspeciesController],
  providers: [EspeciesService],
  exports: [TypeOrmModule],
})
export class EspeciesModule {}
