import { MinLength } from 'class-validator';
import { Raza } from 'src/animales/razas/entities/raza.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Especie {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  @MinLength(2)
  nombre: string;

  @OneToMany(() => Raza, (raza) => raza.especie)
  raza: Raza;
}
