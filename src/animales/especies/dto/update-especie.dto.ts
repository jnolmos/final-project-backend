import { PartialType } from '@nestjs/mapped-types';
import { CreateEspecieDto } from './create-especie.dto';
import { IsString } from 'class-validator';

export class UpdateEspecieDto extends PartialType(CreateEspecieDto) {
  @IsString()
  nombre: string;
}
