import { IsString } from 'class-validator';

export class CreateEspecieDto {
  @IsString()
  nombre: string;
}
