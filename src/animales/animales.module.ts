import { Module } from '@nestjs/common';

import { EspeciesModule } from './especies/especies.module';
import { MascotasModule } from './mascotas/mascotas.module';
import { RazasModule } from './razas/razas.module';

@Module({
  imports: [MascotasModule, RazasModule, EspeciesModule],
})
export class AnimalesModule {}
