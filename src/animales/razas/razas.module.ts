import { Module } from '@nestjs/common';
import { RazasService } from './razas.service';
import { RazasController } from './razas.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Raza } from './entities/raza.entity';
import { EspeciesModule } from '../especies/especies.module';

@Module({
  imports: [TypeOrmModule.forFeature([Raza]), EspeciesModule],
  controllers: [RazasController],
  providers: [RazasService],
})
export class RazasModule {}
