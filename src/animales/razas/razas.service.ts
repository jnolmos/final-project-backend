import { Injectable } from '@nestjs/common';
import { CreateRazaDto } from './dto/create-raza.dto';
import { UpdateRazaDto } from './dto/update-raza.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Raza } from './entities/raza.entity';
import { Repository } from 'typeorm';
import { Especie } from '../especies/entities/especie.entity';

@Injectable()
export class RazasService {
  constructor(
    @InjectRepository(Raza)
    private razaRepository: Repository<Raza>,

    @InjectRepository(Especie)
    private especieRepository: Repository<Especie>,
  ) {}

  async create(createRazaDto: CreateRazaDto) {
    try {
      const especie = await this.especieRepository.findOneBy({
        nombre: createRazaDto.especie,
      });
      if (!especie) throw new Error(`${createRazaDto.especie} no existe.`);

      const nameExists = await this.razaRepository.findOneBy({
        nombre: createRazaDto.nombre,
      });
      if (nameExists) throw new Error(`${createRazaDto.nombre} ya existe.`);

      const newRaza = { ...createRazaDto, especie };

      return this.razaRepository.save(newRaza);
    } catch (error) {
      return error.message;
    }
  }

  findAll() {
    try {
      return this.razaRepository.find();
    } catch (error) {}
  }

  findOne(id: string) {
    try {
      return this.razaRepository.findOneBy({ id });
    } catch (error) {}
  }

  async update(id: number, updateRazaDto: UpdateRazaDto) {
    try {
      const especie = await this.especieRepository.findOneBy({
        nombre: updateRazaDto.especie,
      });
      if (!especie) throw new Error(`${updateRazaDto.especie} no existe.`);
      return this.razaRepository.update(id, { ...updateRazaDto, especie });
    } catch (error) {}
  }

  remove(id: string) {
    try {
      return this.razaRepository.delete(id);
    } catch (error) {}
  }
}
