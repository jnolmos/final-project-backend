import { IsString, MinLength } from 'class-validator';

export class CreateRazaDto {
  @IsString()
  @MinLength(2)
  nombre: string;

  @IsString()
  especie: string;
}
