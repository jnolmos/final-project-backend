import { Especie } from 'src/animales/especies/entities/especie.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Raza {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true, length: 60 })
  nombre: string;

  @ManyToOne(() => Especie, (especie) => especie.raza, { eager: true })
  especie: Especie;
}
